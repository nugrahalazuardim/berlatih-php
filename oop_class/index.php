<?php

    require('animal.php');
    require_once('sheep.php');
    require_once('frog.php');
    require_once('ape.php');

    // release 0
    $sheep = new Sheep("Shaun", "Sheep");

    echo $sheep->get_name();
    echo $sheep->get_kind();
    echo $sheep->get_legs();
    echo $sheep->get_cold_blooded();

    // release 1
    echo "<br>";
    $frog = new Frog("Buduk", "Frog");

    echo $frog->get_name();
    echo $frog->get_kind();
    echo $frog->get_legs();
    echo $frog->get_cold_blooded();
    echo $frog->jump();
    
    echo "<br>";
    $ape = new Ape("Kera Sakti", "Ape");

    echo $ape->get_name();
    echo $ape->get_kind();
    echo $ape->get_legs();
    echo $ape->get_cold_blooded();
    echo $ape->yell();
